package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngineManager;
import java.math.BigDecimal;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {
			if (statement.indexOf("++") != -1 || 
				statement.indexOf("--") != -1 || 
				statement.indexOf("**") != -1 ||
				statement.indexOf("//") != -1 ||
				statement.indexOf("..") != -1 ||
				statement.indexOf(",")  != -1)
			{
				return null;
			}
		
            return new BigDecimal(String.valueOf(new ScriptEngineManager().getEngineByName("JavaScript").eval(statement))).setScale(4, BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString();
        } catch (Exception ex) {
            return null;
        }
    }

}
