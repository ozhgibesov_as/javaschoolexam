package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try
		{
			int size = inputNumbers.size();
			
			int kolvoRows = 1;
			int kolvoElements = 1;
			
			while (kolvoElements < size)
			{
				kolvoRows++;
				kolvoElements = kolvoElements + kolvoRows; 
			}	
			
			int kolvoColumns = kolvoRows * 2 - 1;
			
			int[][] result;
            result = new int[kolvoRows][kolvoColumns];
			
			int nowElement = 0;
			int beginColumn = kolvoRows-1;
			
			for (int i = size-1 ; i > 0 ; i--) {
                for (int j = 0 ; j < i ; j++) {
                    int nowNumber = inputNumbers.get(j);
					int nextNumber = inputNumbers.get(j+1);
					
					if (nowNumber > nextNumber) {
                        inputNumbers.set(j,nextNumber);
						inputNumbers.set(j+1,nowNumber);
                    }
                }
            }

			for (int nowRow = 0; nowRow <= kolvoRows-1; nowRow++) {
                for (int nowIteration = 0; nowIteration <= nowRow; nowIteration++) {
					result[nowRow][beginColumn + 2 * nowIteration] = inputNumbers.get(nowElement);
					nowElement++;
				}

				beginColumn--;
            }
			
			return result;
		} catch (Exception ex) {
            throw new CannotBuildPyramidException();
        }
    }


}
